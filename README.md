# My VIMrc config file.
 A backup of my _vimrc file.

 ## Abstract
 As you may have read on the title, this is just a bakup of my _vimrc file. I created this repo for two reasons:
 * I really wanted to keep some kind of git backup of my _vimrc file.
 * I wanted to upload something to gitlab, in order to get familiar with the whole platform.